package com.coup_demo.coupdemo.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Scooter {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("vin")
    @Expose
    var vin: String? = null
    @SerializedName("model")
    @Expose
    var model: String? = null
    @SerializedName("market_id")
    @Expose
    var marketId: String? = null
    @SerializedName("license_plate")
    @Expose
    var licensePlate: String? = null
    @SerializedName("energy_level")
    @Expose
    var energyLevel: Int? = null
    @SerializedName("distance_to_travel")
    @Expose
    var distanceToTravel: Double? = null
    @SerializedName("location")
    @Expose
    var location: Location? = null

}

