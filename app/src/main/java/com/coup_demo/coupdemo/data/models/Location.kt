package com.coup_demo.coupdemo.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Location {

    @SerializedName("lng")
    @Expose
    var lng: Double? = null
    @SerializedName("lat")
    @Expose
    var lat: Double? = null

}
