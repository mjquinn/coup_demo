package com.coup_demo.coupdemo.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Meta {

    @SerializedName("server_time")
    @Expose
    var serverTime: String? = null
    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("key")
    @Expose
    var key: String? = null

}
