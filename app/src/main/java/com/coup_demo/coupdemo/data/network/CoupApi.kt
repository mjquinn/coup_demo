package com.coup_demo.coupdemo.data.network

import com.coup_demo.coupdemo.data.models.Response
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/**
 * https://app.joincoup.com/api/v1/scooters.json
 * Created by michael on 21/08/2017.
 */
interface CoupApi {

    @GET("scooters.json")
    fun getAllScooters()
            : Observable<Response>
}


class RestApi {

    private val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://app.joincoup.com/api/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    val coupService: CoupApi = retrofit.create(CoupApi::class.java)

}




