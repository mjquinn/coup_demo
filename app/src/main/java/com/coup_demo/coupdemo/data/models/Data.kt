package com.coup_demo.coupdemo.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Data {

    @SerializedName("scooters")
    @Expose
    var scooters: List<Scooter> = emptyList()
}
