package com.coup_demo.coupdemo

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.FragmentActivity
import android.view.View
import com.coup_demo.coupdemo.data.models.Response
import com.coup_demo.coupdemo.data.models.Scooter
import com.coup_demo.coupdemo.data.network.RestApi
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainActivity : FragmentActivity(), OnMapReadyCallback, View.OnClickListener {

    lateinit var mMap: GoogleMap
    private var mBottomSheetBehavior: BottomSheetBehavior<*>? = null
    private val restApi = RestApi()

    private var lowBattery = true
    private var mediumBattery = true
    private var highBattery = true


    private var scooters = emptyList<Scooter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomSheet: View = findViewById(R.id.bottom_sheet)

        val mapSettings = findViewById<View>(R.id.map_settings) as View
        val lowSwitch = findViewById<View>(R.id.low_battery) as View
        val mediumSwitch = findViewById<View>(R.id.medium_battery) as View
        val highSwitch = findViewById<View>(R.id.high_battery) as View

        mapSettings.setOnClickListener(this)
        lowSwitch.setOnClickListener(this)
        mediumSwitch.setOnClickListener(this)
        highSwitch.setOnClickListener(this)

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.low_battery -> {
                lowBattery = !lowBattery
                renderLocationsOnMap()
            }
            R.id.medium_battery -> {
                mediumBattery = !mediumBattery
                renderLocationsOnMap()
            }
            R.id.high_battery -> {
                highBattery = !highBattery
                renderLocationsOnMap()
            }

            R.id.map_settings -> {
                mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)

            }
        }

    }


    fun renderLocationsOnMap() {

        mMap.clear()

        val mapBoundsBuilder = LatLngBounds.Builder()

        scooters.forEach loop@ {

            it.energyLevel?.let {

                if (!lowBattery && it >= 0 && it <= 30) return@loop
                if (!mediumBattery && it >= 31 && it <= 50) return@loop
                if (!highBattery && it >= 50 && it <= 100) return@loop
            }

            val pinColour = pinColour(it.energyLevel)

            it.location?.let {
                val lat = it.lat
                val long = it.lng
                if (lat != null && long != null) {

                    val scooterPin = MarkerOptions().position(LatLng(lat, long))
                    mapBoundsBuilder.include(scooterPin.position)
                    mMap.addMarker(scooterPin).setIcon(BitmapDescriptorFactory.defaultMarker(pinColour))
                }
            }
        }

        try {
            val bounds = mapBoundsBuilder.build()
            val cu = CameraUpdateFactory.newLatLngBounds(bounds, 30)
            mMap.animateCamera(cu)


        } catch (exception: IllegalStateException) {
            //map doesnt have any points
        }
    }


    fun pinColour(energyLevel: Int?): Float {

        when (energyLevel) {
            in 0..30 -> return BitmapDescriptorFactory.HUE_RED
            in 31..50 -> return BitmapDescriptorFactory.HUE_YELLOW
            in 51..100 -> return BitmapDescriptorFactory.HUE_GREEN
            else -> return BitmapDescriptorFactory.HUE_ORANGE
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        val allScooters: Observable<Response> = restApi.coupService.getAllScooters()

        allScooters
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext { response ->
                    scooters = response.data.scooters
                    renderLocationsOnMap()
                }
                .subscribe()
    }
}
